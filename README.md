# Perinet API Specification

The unified API is product agnostic API specification. Each product can be
accessed with a dedicated subset of the API, as it is specified in
this repository. 

The API specification is written according to the OpenAPI Specification [1] (as
well as validated) and is held *RESTful* with minor exceptions (RPC scheme). Its
intended modular structure makes it easy extendable without breaking
compatibility.

In a basic idea each implementing entity implements an applicable subset of the
API Specification. The implementation entities are usually Perinet products,
like periNODEs, periMICA or even periMICA container. Such an instance is also
calle __node__.

The generated documentation is available at the [unified
API](https://perinet.gitlab.io/unified-api) online representation, which
contains:

 * [Base Services API Documentation](https://perinet.gitlab.io/unified-api/base-services_openapi.html)
 * [libperiCORE API Documentation](https://perinet.gitlab.io/unified-api/libperiCORE_openapi.html)
 * [periMICA API Documentation](https://perinet.gitlab.io/unified-api/periMICA_openapi.html)

## Structure of the API Specification

The API specification is a combination of multiple entities, which are called
'__services__' (see [services directory](src/base-services/)). Each service
represents a logically separated functionality of a _node_. Services should be
orthogonal to each other and focus on its functionality. The scope of a
_service_ is intentionally small.

The API of a *service* does not have dependencies to other services, but its
implementation might have. E.g. the
[MQTTClientService](src/base-services/MQTTClient_openapi.yaml) implementation
might depend on the [SecurityService](src/base-services/Security_openapi.yaml)
to acquire the necessary certificates and trust anchors which are necessary to
establish a secured connection to an authentic server.

> A *node* __*must*__ implement at least the
> [NodeService](src/base-services/_Node_openapi.yaml)


## NodeService

The ubiquitous [NodeService](src/base-services/Node_openapi.yaml) is available on each
_node_. It can be used to identify other available services (`/node/services`). 


## General Service

A service is defined as a logical feature set, e.g. `lifecycle` management. The
service implements at least the
[ServiceInfo](src/components/schemas/node.yaml#/ServiceInfo) instance on the
service root path `/<service_name>`. Additional routes beneath the service root
path are service specific.

The Info object of the service, e.g.
[LifeCycleInfo](src/components/schemas/lifecycle.yaml#/LifeCycleInfo), inherits the
[ServiceInfo](src/components/schemas/node.yaml#/ServiceInfo) properties. It provides
therefore at least the property `api_version`, which describes the API version
of the service.


## Development Notes

A devcontainer has been configured for this repository. It is recommended to use
vs-code in combination with a development container
 [[1]](https://code.visualstudio.com/docs/devcontainers/containers) for
development. The included build-targets are tested against the development
container.

The OpenAPI plugin from
[42Crunch](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi)
does not validate properly the specification. It renders opportunistic a
representation. It is recommended to use the
[Redocly](https://marketplace.visualstudio.com/items?itemName=Redocly.openapi-vs-code)
plugin and [redocly-cli](https://github.com/Redocly/redocly-cli)


## References

[1] OpenAPI Specification: [https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md)

[2] Swagger OpenAPI Guide: [https://swagger.io/docs/specification/basic-structure/](https://swagger.io/docs/specification/basic-structure/)