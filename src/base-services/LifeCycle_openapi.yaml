---
openapi: 3.1.0
info:
  title: LifeCycle service RESTful API
  description: >
    Perinet product [openAPI
    definition](https://github.com/OAI/OpenAPI-Specification) [1] of the
    including LifeCycle service.
  version: "VERSION_PLACEHOLDER"
  contact:
    name: Perinet Support
    url: "https://perinet.io/contact"
    email: support@perinet.io
  license:
    name: GNU Affero General Public License (AGPLv3)
    url: "https://www.gnu.org/licenses/agpl-3.0.txt"

tags:
  - name: LifeCycleService
    x-displayName: LifeCycle Service (v23)
    x-apiServiceVersion: 23
    description: |
      The LifeCycle Service is one of the base services of a Perinet node. It
      handles firmware update, factory reset or reboot. Further it enables the
      production mode for re-branding or recalibrating of a product.

      # Changelog

      ### Version 23
        * add error_status to LifeCycleInfo

      ### Version 22
        * Use integer format to represent the `api_version`

      ### Version 21
        * initial release

security:
  - cookieAuth: []
  - mTLS: []


paths:
  /lifecycle:
    get:
      operationId: LifeCycle_Info_Get
      summary: GET LifeCycle Info
      x-access-minimal-role: ADMIN
      tags:
        - LifeCycleService
      description: >
        Provides meta information on the lifeCycle service of a device.
      responses:
        200:
          description: >
            The LifCycle Service info object.
          content:
            application/json:
              schema:
                $ref: "../components/schemas/lifecycle.yaml#/LifeCycleInfo"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        409: #Conflict
          $ref: ../components/responses/error-responses.yaml#/Conflict

  /lifecycle/config:
    get:
      operationId: LifeCycle_Config_Get
      summary: GET LifeCycle Config
      x-access-minimal-role: ADMIN
      tags:
        - LifeCycleService
      description: >
        Provides configuration properties of the LifeCycle service.
      responses:
        200:
          description: >
            The LifCycle Service info object.
          content:
            application/json:
              schema:
                $ref: "../components/schemas/lifecycle.yaml#/LifeCycleConfig"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict
    patch:
      operationId: LifeCycle_Config_Patch
      summary: PATCH LifeCycle Config
      x-access-minimal-role: ADMIN
      x-product-states:
        - PRODUCTION
        - OEM
      tags:
        - LifeCycleService
      description: >
        Set configuration properties of the LifeCycle service.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "../components/schemas/lifecycle.yaml#/LifeCycleConfig"
      responses:
        202:
          description: >
            The LifCycle Config object was accepted and is going to be stored.
        204:
          description: |
            The request has been received and the dedicated action is finished.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict

  /lifecycle/firmware/production-image:
    get:
      operationId: LifeCycle_FirmwareProductionImageStatus_Get
      summary: GET OEM Production FirmwareImage Status
      x-access-minimal-role: ADMIN
      x-product-states:
        - PRODUCTION
      tags:
        - LifeCycleService
      responses:
        200:
          description: >
            Returns the current status of the firmware update process, initiated
            on the PUT method on this resource.
          content:
            application/json:
              schema:
                $ref: "../components/schemas/lifecycle.yaml#/LifeCycleFwStatus"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict
    put:
      operationId: LifeCycle_FirmwareProductionImage_Set
      summary: PUT OEM Production FirmwareImage
      x-access-minimal-role: ADMIN
      x-product-states:
        - PRODUCTION
      tags:
        - LifeCycleService
      description: |
        Update the production firmware image of a Perinet product.

        __NOTE__: This function might conflict with the state of the Perinet
        product. In the case of a periCORE based product, only with the periCORE
        firmware the update of the production firmware image is not in conflict
        with the product state and is only available in product state
        PRODUCTION. Please be referred to the product specific documentation,
        e.g. [periCORE
        Datasheet](https://docs.perinet.io/PRN100375%20-%20periCORE%20Datasheet.pdf),
        [periMICA
        UserGuide](https://docs.perinet.io/PRN100392%20-%20periMICA%20User%20Guide.pdf)
      parameters: []
      requestBody:
        $ref: "#/components/requestBodies/FirmwareImage"
      responses:
        202:
          description: The firmware has been received and the update is in progress.
        204:
          description: |
            The request has been received and the dedicated action is finished.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict

  /lifecycle/firmware/update-image:
    get:
      operationId: LifeCycle_FirmwareUpdateImage_Get
      summary: GET Firmware Update Image Status
      x-access-minimal-role: ADMIN
      x-product-states:
        - OEM
        - UPDATE
      tags:
        - LifeCycleService
      responses:
        200:
          description: >
            Returns the current status of the firmware update process, initiated
            on the PUT method on this resource.
          content:
            application/json:
              schema:
                $ref: "../components/schemas/lifecycle.yaml#/LifeCycleFwStatus"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict

    put:
      operationId: LifeCycle_FirmwareUpdateImage_Set
      summary: PUT Firmware Update Image
      x-access-minimal-role: ADMIN
      x-product-states:
        - OEM
        - UPDATE
      tags:
        - LifeCycleService
      description: |
        Update the firmware image of a Perinet product.

        __NOTE__: This function might conflict with the state of the Perinet
        product. In the case of a periCORE based product, only with the periCORE
        firmware the update of the production firmware image is not in conflict
        with the product state. Please be referred to the product specific
        documentation, e.g. [periCORE
        Datasheet](https://docs.perinet.io/PRN100375%20-%20periCORE%20Datasheet.pdf),
        [periMICA
        UserGuide](https://docs.perinet.io/PRN100392%20-%20periMICA%20User%20Guide.pdf)

      requestBody:
        $ref: "#/components/requestBodies/FirmwareImage"
      responses:
        202:
          description: >
            The update firmware image has been received and the update is in progress.
        204:
          description: |
            The request has been received and the dedicated action is finished.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        405:
          $ref: ../components/responses/error-responses.yaml#/NotAllowed
        409:
          $ref: ../components/responses/error-responses.yaml#/Conflict

  /lifecycle/firmware/soft-reboot:
    patch:
      operationId: LifeCycle_Reboot_Set
      summary: PATCH Soft Reboot
      x-access-minimal-role: ADMIN
      description: >
        The device reboot process is invoked.
      requestBody:
        $ref: "../components/requestBodies/basics.yaml#/Empty"
      responses:
        202:
          description: >
            Request has been accepted and the operation has been scheduled.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
      tags:
        - LifeCycleService

  /lifecycle/factory-reset:
    patch:
      operationId: LifeCycle_FactoryReset_Set
      summary: PATCH Factory Reset
      x-access-minimal-role: ADMIN
      description: >
        Restores the factory default settings of the device.
      requestBody:
        $ref: "../components/requestBodies/basics.yaml#/Empty"
      responses:
        202:
          description: The request has been received and the dedicated action is ongoing.
        204:
          description: |
            The request has been received and the dedicated action is finished.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
      tags:
        - LifeCycleService

components:
  securitySchemes:
    $ref: "../components/security-schemes.yaml"
  requestBodies:
    FirmwareImage:
      required: true
      content:
        application/vnd-perinet-fwimage:
          schema:
            $ref: "../components/schemas/lifecycle.yaml#/FirmwareImage"
