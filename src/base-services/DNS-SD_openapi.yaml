---
openapi: 3.1.0
info:
  title: DNS-SD service RESTful API
  description: >
    Perinet product [openAPI
    definition](https://github.com/OAI/OpenAPI-Specification) [1] of the
    including DNS-SD service.
  version: "VERSION_PLACEHOLDER"
  contact:
    name: Perinet Support
    url: "https://perinet.io/contact"
    email: support@perinet.io
  license:
    name: GNU Affero General Public License (AGPLv3)
    url: "https://www.gnu.org/licenses/agpl-3.0.txt"

security:
  - cookieAuth: []
  - mTLS: []

tags:
  - name: DNSSDService
    x-displayName: DNS-SD Service (v24)
    x-apiServiceVersion: 24
    x-traitTag: false
    description: |
      The [DNS Service Discovery](http://www.dns-sd.org) service can be used to
      discover remote network services or advertise local services to the
      network. Services are discovered via the IPv6 mDNS broadcast protocol,
      which reaches services in the link local domain only. Means, it __will not__
      discover services beyond a routing node.

      > **NOTE**: since api version 23, an implementation of the DNSSDService
      > must announce the `_https._tcp` service with the specified "DNS-SD
      > TXTRecord (_https._tcp)".
     

      # Changelog
      
      ### Version 24
        * change mandatory behaviour an implementation: `DNSSD_ServiceInfo_Discovered_Get`
        * `DNSSD_ServiceInfo_Discovered_Get`: add SSE response option
        * `DNSSD_ServiceInfo_Discovered_Get`: add 204 response option
        * `DNSSD_ServiceInfo_Discovered_Get`: add implementation behaviour to spec
        * `DNSSD_ServiceInfo_Discovered_Set`: removed
        * `DNSSDHTTPSTXTRecord`: rename field `id` to `format` to prevent ambiguity

      ### Version 23
        * DNS-SD announced `_https._tcp` service, with a minimal but mandatory set of properties.
        * add error_status to `DNSSDInfo`

      ### Version 22
        * Use integer format to represent the `api_version`

      ### Version 21
        * initial release

paths:
  /dns-sd:
    get:
      operationId: DNS-SD_Info_Get
      summary: Get DNS-SD Info object
      description: >
        Provides meta information of the DNS-SD service.
      # ../components/schemes/security.yaml#/SecurityAccessRole
      x-access-minimal-role: USER
      tags:
        - DNSSDService
      responses:
        200:
          description: >
            Latest state of the DNS-SD info object.
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "../components/schemas/dns-sd.yaml#/DNSSDInfo"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden

  /dns-sd/advertised-services:
    get:
      operationId: DNSSD_ServicesInfo_Advertise_Get
      summary: GET Advertised DNS-SD Services
      description: >
        A list of names of the advertised dns-sd services from the node.
      # ../components/schemes/security.yaml#/SecurityAccessRole
      x-access-minimal-role: ADMIN
      tags:
        - DNSSDService
      responses:
        200:
          description: >
            A list of service names that are advertised by the node
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceNames"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden

  /dns-sd/advertised-services/{service_name}:
    parameters:
      - name: service_name
        in: path
        required: true
        style: simple
        schema:
          $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceName"
    get:
      operationId: DNSSD_ServiceInfo_Advertise_Get
      summary: GET Advertised DNS-SD Service Info object
      x-access-minimal-role: ADMIN
      tags:
        - DNSSDService
      responses:
        200:
          description: >
            DNS-SD Info Object of a particular service name
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceInfo"
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        404:
          description: >
            Requested {service_name} was not found or is unknown to the API
            implementing entity.
    patch:
      operationId: DNSSD_ServiceInfo_Advertise_Set
      summary: SET Advertised DNS-SD Service Info object
      # ../components/schemes/security.yaml#/SecurityAccessRole
      x-access-minimal-role: INTERNAL
      tags:
        - DNSSDService
      requestBody:
        content:
          application/json:
            schema:
              $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceInfo"
      responses:
        202:
          description: >
            The DNSSDServiceInfo object has been received and will be updated.
        204:
          description: >
            The DNSSDServiceInfo object has been updated.
        400:
          $ref: ../components/responses/error-responses.yaml#/BadRequest
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        404:
          description: >
            Requested {service_name} was not found or is unknown to the API
            implementing entity.
    delete:
      operationId: DNSSDServiceInfoAdvertiseRemove
      summary: DELETE Advertised DNS-SD Service Info object
      # ../components/schemes/security.yaml#/SecurityAccessRole
      x-access-minimal-role: INTERNAL
      tags:
        - DNSSDService
      responses:
        200:
          description: >
            The DNSSDServiceInfo object has been removed.
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        404:
          description: >
            Requested {service_name} was not found or is unknown to the API
            implementing entity.

  /dns-sd/discovered-services:
    post:
      operationId: DNSSDServiceInfoDiscoverSet
      summary: (REMOVED) Start Discovery of DNS-SD Service Type
      description: |
        Start mDNS / DNS-SD service browsing for topic {service_name}. The
        browsing will stop after a timeout of 30 seconds and the resource will
        be deleted.
      # ../components/schemes/security.yaml#/SecurityAccessRole
      x-access-minimal-role: USER
      x-maximal-api-version: 23
      x-removed: true
      tags:
        - DNSSDService
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                service_name:
                  $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceName"
      responses:
        # 201:
        #   description: >
        #     The Service Info object of a particular {id}
        #   content:
        #     application/json:
        #       schema:
        #         $ref: "../components/schemas/dns-sd.yaml#/DNSSDBrowserInfo"
        # 401:
        #   $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        # 403:
        #   $ref: ../components/responses/error-responses.yaml#/Forbidden
        # 404:
        #   description: >
        #     Requested {service_name} was not found or is unknown to the API
        #     implementing entity.
        501:
          x-summary: Not Implemented
          x-minimal-api-version: 24
          description: this resource has been removed in api-version 24

  /dns-sd/discovered-services/{service_name}:
    parameters:
      - name: service_name
        in: path
        required: true
        style: simple
        schema:
          $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceName"
    get:
      operationId: DNSSD_ServiceInfo_Discovered_Get
      summary: GET Discovered DNS-SD Service Info objects
      description: |
        Get a list of services for {service_name}.

        > __Note__: The following behaviour must be provided by an implementation (api-version:24):
        >   * Implicit activate on first invocation.
        >   * Implicit keep activated with kept alive connection (SSE)
        >   * Implicit deactivate with timeout 30sec or closed connection (SSE)
      x-access-minimal-role: USER
      tags:
        - DNSSDService
      responses:
        200:
          description: |
            Returns a list or a server sent event
            ([SSE](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events))
            stream with [Data-only
            messages](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#examples).,
            which contain one or more `DNSSDServiceInfo` objects of a particular
            {service_name}.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceInfo"
            text/event-stream:
              x-minimal-api-version: 24
              example: |
                data: {"type":"_https._tcp","domain":"local.","priority":0,"weight":0,"ttl":300,"port":443,"hostname":"periNODE-sernm.local","name":"periNODE-sernm","addresses":["fe80::762e:db02:3d8b:0000","2a01:1234:2:1::db02:3d8b:0000","192.168.0.66"],"txt_record":{"format":"uAPI","api_version":24,"application_name":"string","element_name":"string","product_name":"periCORE","error_status":"WARNING"}}
                data: {"type":"_https._tcp","domain":"local.","priority":0,"weight":0,"ttl":300,"port":443,"hostname":"periNODE-sernm.local","name":"periNODE-sernm","addresses":["fe80::762e:db02:3d8b:0000","2a01:1234:2:1::db02:3d8b:0000","192.168.0.66"],"txt_record":{"format":"uAPI","api_version":24,"application_name":"string","element_name":"string","product_name":"periCORE","error_status":"WARNING"}}
                data: {"type":"_https._tcp","domain":"local.","priority":0,"weight":0,"ttl":300,"port":443,"hostname":"periNODE-sernm.local","name":"periNODE-sernm","addresses":["fe80::762e:db02:3d8b:0000","2a01:1234:2:1::db02:3d8b:0000","192.168.0.66"],"txt_record":{"format":"uAPI","api_version":24,"application_name":"string","element_name":"string","product_name":"periCORE","error_status":"WARNING"}}
                data: {"type":"_https._tcp","domain":"local.","priority":0,"weight":0,"ttl":300,"port":443,"hostname":"periNODE-sernm.local","name":"periNODE-sernm","addresses":["fe80::762e:db02:3d8b:0000","2a01:1234:2:1::db02:3d8b:0000","192.168.0.66"],"txt_record":{"format":"uAPI","api_version":24,"application_name":"string","element_name":"string","product_name":"periCORE","error_status":"WARNING"}}
              schema:
                type: array
                items:
                  type: object
                  description: |
                    [Data-only messages](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#examples) of server sent events
                  properties:
                    data:
                      $ref: "../components/schemas/dns-sd.yaml#/DNSSDServiceInfo"
        204:
          x-minimal-api-version: 24
          description: |
            No data for {service_name} has been received. Service discovery is on-going.
        400:
          description: |
            Error in the request. This might happen, when the parameter
            {serivice_name} does not comply to
            [rfc6763](https://datatracker.ietf.org/doc/html/rfc6763)
          x-minimal-api-version: 24
        401:
          $ref: ../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../components/responses/error-responses.yaml#/Forbidden
        404:
          x-maximal-api-version: 23
          x-removed: true
          description: |
            >__NOTE__: removed with API_VERSION 24
            
            ~~Requested {id} was not found or is unknown to the API implementing
            entity. This happens when the time_out of 30sec is expired.~~

components:
  securitySchemes:
    $ref: "../components/security-schemes.yaml"
