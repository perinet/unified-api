---
openapi: 3.1.0
info:
  title: Proxy service RESTful API
  description: >
    Perinet product [openAPI
    definition](https://github.com/OAI/OpenAPI-Specification) [1] of the
    Proxy service. 
  version: "VERSION_PLACEHOLDER"
  contact:
    name: Perinet Support
    url: "https://perinet.io/contact"
    email: support@perinet.io
  license:
    name: GNU Affero General Public License (AGPLv3)
    url: "https://www.gnu.org/licenses/agpl-3.0.txt"

externalDocs: 
  url: 'https://docs.perinet.io'
  description: Perinet product documentation online storage.


tags:
  - name: ProxyService
    description: Proxy Service

security: 
  - cookieAuth: []
  - mTLS: []
  
paths:
  /proxy:
    get:
      operationId: Proxy_Info_Get
      summary: ProxyInfo object
      tags:
        - ProxyService
      responses:
        200:
          description: >
            Service Information
          content:
            application/json:
              schema:
                $ref: '../../components/schemas/proxy.yaml#/ProxyInfo'
        401:
          $ref: ../../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../../components/responses/error-responses.yaml#/Forbidden
    delete:
      operationId: Proxy_Delete_All
      summary: remove all proxy entries
      tags:
        - ProxyService
      responses:
        204:
          description: >
            All proxy entries are removed
        401:
          $ref: ../../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../../components/responses/error-responses.yaml#/Forbidden

  /proxy/{service}:
    parameters:
      - $ref: '#/components/parameters/Proxy'
    patch:
      operationId: Proxy_Set
      summary: add or exchange a proxy entry
      tags:
        - ProxyService
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '../../components/schemas/proxy.yaml#/ProxyConfig'
      responses:
        204:
          description: >
            Proxy will be created
        401:
          $ref: ../../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../../components/responses/error-responses.yaml#/Forbidden
    delete:
      operationId: Proxy_Delete
      summary: remove a proxy entry of dedicated name {service}
      tags:
        - ProxyService
      responses:
        202:
          description: >
            The proy entry has been found and will be removed
        401:
          $ref: ../../components/responses/error-responses.yaml#/UnAuthorized
        403:
          $ref: ../../components/responses/error-responses.yaml#/Forbidden
        404:
          description: >
            Requested {Proxy} was not found or is unknown to the API
            implementing entity.

components: 
  securitySchemes:
    $ref: '../../components/security-schemes.yaml'
  parameters:
    Proxy:
      name: service
      in:  path
      required: true
      style: simple
      description: >
        The service name to proxy
      schema:
        type: string
