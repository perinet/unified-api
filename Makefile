# Copyright Perinet GmbH. All Rights Reserved.
# 
# This software is dual-licensed: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation. For the terms of this
# license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
# or contact us at https://perinet.io/contact/ when you want license
# this software under a commercial license.

# Default location for build targets
BDIR ?= $(PWD)/_build
# Default location for install targets
DEST ?= $(PWD)/_install

BUILD_DIR = $(BDIR)/unified-api
DEST_DIR = $(DEST)/unified-api

VERSION ?= $(shell git rev-parse --abbrev-ref HEAD)-$(shell date -u --iso-8601=minutes)

export VERSION

.DEFAULT: install

%:
	$(MAKE) -C src $@ BDIR=$(BUILD_DIR) DEST=$(DEST_DIR) VERSION=$(VERSION)

deploy: install
	cd $(DEST) \
	&& zip -r unified-api-libperiCORE.zip unified-api/libperiCORE* \
	&& zip -r unified-api-periMICA.zip unified-api/periMICA* \
	&& zip -r unified-api-base-services.zip unified-api/base-services* \

distclean:
	rm -rf "$(DEST_DIR)"
	rm -rf "$(BUILD_DIR)"
